package pl.edu.pw.elka.stud.abielaw1.morfologik

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

import org.carrot2.text.linguistic.morfologik.MorfologikStemmerAdapter

import pl.edu.pw.elka.stud.abielaw1.stemmer.StemmerApp

/** Obiekt-adapter dla stemmera Morfologik. */
object MorfologikWrapper extends StemmerApp {
  lazy val stemmer = new MorfologikStemmerAdapter with CanStem
}
