# Porównanie dwóch stemmerów/lematyzatorów

## Budowanie

Do zbudowania dokumentacji należy wykonać polecenie:

    mvn -pl comparator latex:latex

wyjściowy plik PDF powinien być zlokalizowany w katalogu `comparator/target`.

Do zbudowania programu należy wykonać polecenie:

    mvn package

Adaptery stemmerów zostaną umieszczone w następujących miejscach:

  - *morfologik* - `morfologik-wrapper/target/morfologik-wrapper-1.0.jar`
  - *hunspell*   - `hunspell-wrapper/target/hunspell-wrapper-1.0.jar`
  - *stempel*    - `stempel-wrapper/target/stempel-wrapper-1.0.jar`

Komparator jest w następującej lokalizacji:
`comparator/target/comparator-1.0.jar`

## Użycie komparatora

Komparator uruchamiany jest za pomocą poniższego polecenia:

    java -jar comparator/target/comparator-1.0.jar [pliki] [wejściowe] ...

rezultat porównania przekazywany jest na wyjście standardowe. Pliki wejściowe
muszą być w formacie zwracanym przez adaptery stemmerów. W przypadku ręcznej
modyfikacji tych plików należy pamiętać o nieumieszczaniu pustych grup.

## Użycie adapterów stemmerów

Każdy adapter uruchamiany jest poleceniem:

    java -jar [adapter]/target/[adapter]-1.0.jar <[dokument wejściowy]

dokument wejściowy (zwykły tekst w jęz. polskim) musi być przekazany na wejście
standardowe. Wynik przekazawny jest na wyjście standardowe.

## Użycie narzędzia do wprowadzania szumu

    java -jar noiser/target/noiser-1.0.jar <[dokument wejściowy]

dokument wejściowy (dokument zwrócony przez adapter stemmera) musi być
przekazany przez wejście standardowe. Wynik przekazawny jest na wyjście
standardowe i jest on w formacie takim samym jak wyjście generowane przez
adaptery stemmerów.

## Uruchomienie testowe

Aby w prosty sposób uruchomić programy w odpowiedniej sekwencji można posłużyć
się skryptami umieszczonymi w katalogu `src/main/scripts`. Są to skrypty powłoki
GNU Bash.

    bash src/main/scripts/run.sh [dokument wejściowy]

jako dokument wejściowy oczekiwany jest dowolny plik z tekstem w jęz. polskim.
Skrypt uruchamia kolejno adaptery stemmerów: Hunspell, Morfologik i Stempel.
Rezultaty przekazywane są dodatkowo do plików `/tmp/[nazwaStemmera].xml`.
Następnie wykonywane jest porównanie tych wyników, a jego rezultat umieszczany
jest w pliku `/tmp/cmp.xml`. Na standardowe wyjście przekazywane są jego
elementy opisujące miary jakości dla stemmerów. Miary te pokazywane są zawsze
w tej samej kolejności: dla stemmerów Hunspell, Morfologik oraz Stempel.
Ustalona kolejność pozwala na identyfikację stemmera mimo nieczytelnej
nazwy pliku wejściowego przekazywanej programowi przez system operacyjny.

Dla testów z wynikami "zaszumionymi" można posłużyć się podobnym skryptem:

    bash src/main/scripts/run-with-noise.sh [poziom 0-100] [dokument wejściowy]

różnica polega na tym, że wykorzystywany jest tylko jeden adapter (dla
najszybszego stemmera Stempel), natomiast dodane są dokumenty będące wynikami
poddania pierwszego mutacji z poziomem intensywności zadanym jako parametr.

Przykładowe wejście umieszczone zostało w katalogu `src/main/resources`.
