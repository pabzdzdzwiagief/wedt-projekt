package pl.edu.pw.elka.stud.abielaw1.comparator

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

import scala.xml.XML
import pl.edu.pw.elka.stud.abielaw1.comparator.groups.WordGroupFactory
import pl.edu.pw.elka.stud.abielaw1.comparator.groups.GroupSimilarity
import scala.xml.PrettyPrinter

/** Obiekt aplikacji komparatora. */
object Comparator extends App {
  /** Pary nazw i zawartości wczytanych plików wejściowych. */
  val inputs = for (argument <- args) yield {
    val groupDocument = XML loadFile argument
    val maybeWordGroups = for (group <- groupDocument.child) yield
                            WordGroupFactory fromXML group
    (argument, maybeWordGroups.flatten)
  }

  /** Wyniki porównania stemmerów ze sobą. */
  val results = for {
    (name, input) <- inputs // dla każdego rezultatu stemmera ...
    otherInputs = inputs map (_._2) filterNot (_ eq input)
    // ... dla każdej grupy z znajdowana jest najbliższa grupa z innych wyników
    groupSimilarities = input map { g =>
        GroupSimilarity(g, (otherInputs map g.mostSimilar): _*)
    }
    result = Result(name, groupSimilarities: _*)
  } yield result.toXML

  /** Wyniki wraz z elementem-korzeniem. */
  val fullResult = <results>{results}</results>

  /** Sformatowany XML. */
  val prettyResult = new PrettyPrinter(80, 4).format(fullResult)

  println(prettyResult)
}
