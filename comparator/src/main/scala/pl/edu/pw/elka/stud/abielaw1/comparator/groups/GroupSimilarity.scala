package pl.edu.pw.elka.stud.abielaw1.comparator.groups

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

/**
 * Zbiór podobieństw różnych grup do danej grupy.
 * @param to - grupa, do której wyliczono podobieństwa.
 * @param similarities - grupy (z innych stemmerów).
 */
final case class GroupSimilarity(to: WordGroup, similarities: Similarity*) {
  /** Średnie podobieństw do danej grupy. */
  lazy val averageSimilarity = {
    val sum = similarities.map(_.percentage).sum
    val num = similarities.size
    if (num > 0) sum/num else 0
  }
}