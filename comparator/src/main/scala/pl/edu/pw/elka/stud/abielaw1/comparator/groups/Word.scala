package pl.edu.pw.elka.stud.abielaw1.comparator.groups

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

/**
 * Pełne słowo.
 * @param text - treść słowa.
 */
final case class Word(text: String)