package pl.edu.pw.elka.stud.abielaw1.comparator.groups

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

import scala.xml.Node

/** Obiekt wytwarzający obiekty reprezentujące grupy słów z dokumentów XML. */
object WordGroupFactory {
  def fromXML(groupTag: Node): Option[WordGroup] = groupTag match {
    case group @ <group>{_*}</group> =>
      val stem = Stem((group \ "@stem").text)
      val words = for (wordTag <- group \ "word") yield
                    Word(wordTag.text)
      Some(WordGroup(stem, words.toSet))
    case _ => None
  }
}
