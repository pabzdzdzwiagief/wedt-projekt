package pl.edu.pw.elka.stud.abielaw1.comparator.groups
import scala.math.Ordered

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

/**
 * Reprezentacja podobieństwa dwóch grup.
 * @param of - podobieństwo czego ...
 * @param to - ... do czego (podobieństwo jest zwrotne).
 * @param percentage - procentowa wartość podobieństwa.
 */
final case class Similarity(of: WordGroup, to: WordGroup, percentage: Int)
    extends Ordered[Similarity] {
  def compare(that: Similarity) = this.percentage compare that.percentage
}
