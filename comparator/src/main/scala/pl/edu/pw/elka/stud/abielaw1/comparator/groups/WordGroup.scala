package pl.edu.pw.elka.stud.abielaw1.comparator.groups

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

import scala.math.sqrt

/**
 * Rdzeń i skojarzone z nim słowa (tj. słowa, dla których stemmer znalazł
 * ten sam stem).
 * @param stem - wspólny dla wszystkich słów rdzeń.
 * @param stemmed - słowa, dla których znaleziono ten sam rdzeń.
 */
final case class WordGroup(stem: Stem, stemmed: Set[Word]) {
  /** Miara cosinusowa podobieństwa do innej grupy. Wyrażana w procentach. */
  def similarityWith(that: WordGroup): Similarity = {
    val scalarProduct = (this.stemmed & that.stemmed).size
    val normalization = sqrt(this.stemmed.size * that.stemmed.size).toInt
    Similarity(this, that, (scalarProduct * 100)/normalization)
  }

  /** @return podobieństwo najbardziej podobnej do tej grupy. */
  def mostSimilar(thats: Traversable[WordGroup]): Similarity = {
    val similarities = thats map similarityWith
    similarities.max
  }
}
