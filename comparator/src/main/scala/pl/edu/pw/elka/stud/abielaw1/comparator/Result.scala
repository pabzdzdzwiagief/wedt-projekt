package pl.edu.pw.elka.stud.abielaw1.comparator

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

import scala.xml.{Attribute, Text, Null}

import pl.edu.pw.elka.stud.abielaw1.comparator.groups.GroupSimilarity

/**
 * Wynik porównania jednego stemmera z pozostałymi.
 * @param name nazwa stemmera
 * @param similarities podobieństwa pomiędzy kolejnymi grupami
 */
final case class Result(name: String, similarities: GroupSimilarity*) {
  /** Sprowadza dane do postaci XML. */
  lazy val toXML =
    <result stemmer={name} averageSimilarity={similarity + "%"}>
      {for (group <- similarities) yield
      <group stem={group.to.stem.text}>
        {for (similarityToOther <- group.similarities) yield
        <similarity stem={similarityToOther.to.stem.text}>
          {similarityToOther.percentage + "%"}
        </similarity>}
      </group>}
    </result>

  /** Średnie podobieństwo grup z tego stemmera do grup z innych. */
  private lazy val similarity = {
    val sum = similarities.map(_.averageSimilarity).sum
    val num = similarities.size
    sum / num
  }
}
