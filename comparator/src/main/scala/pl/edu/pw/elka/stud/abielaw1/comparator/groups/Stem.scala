package pl.edu.pw.elka.stud.abielaw1.comparator.groups

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

/**
 * Rdzeń słowa.
 * @param text - treść rdzenia.
 */
final case class Stem(text: String)