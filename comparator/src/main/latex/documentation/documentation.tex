\NeedsTeXFormat{LaTeX2e}

\documentclass[12pt,a4paper,oneside]{mwart}

\input{packages}

\title{
  Projekt z przedmiotu WEDT\\
  Porównanie stemmerów
}

\author{Aleksander Bielawski, A.J.Bielawski@stud.elka.pw.edu.pl}
\date{\today}

\begin{document}

\maketitle

\section*{Cel projektu}
Celem projektu jest stworzenie narzędzia pozwalającego na porównywanie ze sobą
różnych stemmerów. Wynikiem porównania ma być informacja o mierze jakości
przypisanej do poszczególnych stemmerów. Miara ta ma być określana na podstawie
podobieństwa wyników uzyskiwanych przez porównywane stemmery. Metoda zakłada, że
im bardziej podobne do innych wyniki wyprowadza dany stemmer, tym większa jest
szansa, że wyniki te nie zostały osiągnięte przypadkowo.

\section*{Struktura źródeł projektu}
Projekt jest napisany w języku Scala. Źródła projektu składają się
z kilku modułów odpowiadających za określone składowe projektu:

\begin{itemize}
\item \textbf{stemmer} - moduł odpowiający za procedurę sprowadzania wyjścia
                         testowanego stemmera do wspólnego formatu.
\item \textbf{hunspell-wrapper} - adapter stemmera z projektu Hunspell.
\item \textbf{morfologik-wrapper} - adapter stemmera Morfologik.
\item \textbf{stempel-wrapper} - adapter stemmera Stempel.
\item \textbf{noiser} - narzędzie do wprowadzania losowych modyfikacji
                        w dokumencie wyjściowym adaptera stemmera.
\item \textbf{comparator} - właściwe narzędzie porównujące wyniki stemmerów.
\end{itemize}

\section*{Budowanie i testowanie projektu}
Jako narzędzie do budowy projektu wybrane zostało narzędzie Apache Maven.
Do złożenia umieszczonej w źródłach niniejszej dokumentacji potrzebny jest
system składu dokumentów \LaTeX. Do uruchomienia skryptów testowych wymagana
jest powłoka GNU Bash i środowisko uniksopodobne.

Opis budowania i uruchamiania poszczególnych składowych projektu umieszczony
jest w pliku \texttt{README.md}.

\section*{Metoda przeprowadzania porównania}
W celu dokonania porównania wykonywane są następujące czynności:
\begin{enumerate}
\item Każdemu stemmerowi zadawany jest ten sam dokument.
\item Adaptery stemmerów generują grupowania słów uzyskiwane w wyniku pracy
      stemmerów.
\item \textit{(opcjonalnie)} Niektóre wyniki poddawane są mutacjom.
\item Rezultaty porównanywane są za pomocą komparatora.
\item Komparator generuje wyniki określające szacowane jakości stemmerów.
\end{enumerate}

\begin{figure}[htb]
    \centering
    \includegraphics[width=0.8\textwidth]{diagram.pdf}
    \caption{Schemat przeprowadzania porównania}
\end{figure}

\subsection*{Grupowanie słów}
W celu dokonania porównania stemmerów wygodne jest sprowadzenie wyjścia każdego
stemmera do wspólnego formatu. W tym celu wykorzystane są adaptery stemmerów.
Generują one wyjście w postaci dokumentu XML zawierającego grupy słów. Każda
grupa składa się z wyrazów, dla których stemmer odnalazł wspólny rdzeń.
Informacja o tym rdzeniu również jest zawarta w dokumencie.

\begin{verbatim}
<results>
    <group stem=”projekt”>
        <word>projektu</word>
        <word>projektem</word>
    </group>
    <group stem=”stemmer”>
        <word>stemmera</word>
        <word>stemmerami</word>
        <word>stemmerem</word>
    </group>
</results>
\end{verbatim}

\subsection*{Porównanie}
Rezultaty działania stemmerów sprowadzone do jednorodnej postaci mogą być ze
sobą porównane. Problemem pojawiającym się przy próbie porównania różnych
wyników jest prawdopodobna niezgodność odnalezionych rdzeni. Naiwne dopasowanie
grup na podstawie identyczności rdzeni może zakończyć się niepowodzeniem także
dla dwóch identycznie grupujących wyrazy stemmerów.

Aby uzyskiwać wiarygodne wyniki potrzebne jest dopasowywanie grup na podstawie
podobieństwa ich zawartości. Jeśli dwie grupy składają się z tych samych słów,
to oznacza to, że te grupy odnalezione przez różne stemmery są sobie
odpowiadające. W realnych przypadkach mogą zdarzać się jednak grupy nie
identyczne, ale różniące się niewielką ilością elementów. Do radzenia sobie
z nimi potrzebne jest wykorzystanie pewnej miary podobieństwa. W tym projekcie
zastosowano miarę cosinusową podobieństwa grup.

Dla każdego rezultatu stemmera szacowane jest podobieństwo zawartych w nim grup
do grup uzyskiwanych przez pozostałe stemmery. Dla każdej grupy wyszukiwane są
w wynikach innych stmmerów takie, które są najbardziej podobne. W ten sposób
grupa otrzymuje skojarzenia z pozostałymi, po jednym dla każdego z pozostałych
stemmerów. Wyliczana jest średnia z podobieństw do znalezionych dopasowań.
Średnia ta pozwala oszacować, na ile dana grupa jest ,,odstająca'' od innych
znajdujących się w wynikach stemmerów. Średnia takich oszacowań stanowi
oszacowanie jakości samego stemmera.

\subsection*{Wyniki}
Wynik porównania przekazywany jest w postaci, której przykładowy fragment
przedstawiony jest poniżej:

\begin{verbatim}
<result averageSimilarity="92%" stemmer="/dev/fd/63">
  <group stem="będących">
    <similarity stem="być"> 33% </similarity>
    <similarity stem="będący"> 100% </similarity>
  </group>
  <group stem="tych">
    <similarity stem="ten"> 50% </similarity>
    <similarity stem="tych"> 100% </similarity>
  </group>
  ...
\end{verbatim}

Atrybut \texttt{averageSimilarity} oznacza szacowaną jakość stemmera.
Elementy \texttt{group} zawierają podobieństwa grup z innych stemmerów
najpodobniejszych do danej grupy. Podobieństwa te występują zawsze zgodnie
z kolejnością przekazywania wyników do komparatora, tj. dla grup podobnych do
grupy odnalezionej przez stemmer 1. podana będzie najpierw grupa podobna
uskana przez stemmer 2., następnie grupa ze stemmera 3. itd.

\section*{Analiza wrażliwości}
W celu ułatwienia oceny wiarygodności porównania w ramach projektu opracowano
narzędzie do wprowadzania mutacji do wyników działania stemmerów. Narzędzie to
pozwala na automatyzację i parametryzację intensywności wprowadzania mutacji.
Jeśli komparator potrafi wyróżnić lepszy stemmer, to powinien on wskazać również
wynik stemmera jako lepszy od jego mutacji.

\subsection*{Mutacje}
Narzędzie przeprowadza następujące mutacje:
\begin{itemize}
\item usunięcie losowego słowa z grupy.
\item przeniesienie losowego słowa z jednej grupy do drugiej.
\item złączenie ze sobą losowej pary grup.
\end{itemize}

\subsection*{Wejście testowe}
Jako dane wejściowe wykorzystane przy porównaniu użyto przetworzone dane
zaczerpnięte z \cite{wiki} umieszczone w pliku \texttt{legal.txt} oraz
z \cite{dict} umieszczone w pliku \texttt{dictionary.txt} (użyto pierwsze
10000 słów z tego zbioru).

\subsection*{Wyniki testowania}
Poniżej przedstawione są wyniki porównania stemmerów: Hunspell, Morfologik
i Stempel na wybranych danych testowych.
\begin{center}
    \begin{tabular}{ | l | c | c | }
    \hline
               & \texttt{legal.txt}   & \texttt{dictionary.txt} \\ \hline
    Morfologik & 97\%                 & 90\%                    \\ \hline
    Hunspell   & 92\%                 & 85\%                    \\ \hline
    Stempel    & 89\%                 & 52\%                    \\ \hline
    \end{tabular}
\end{center}
Wyniki odpowiadają intuicji: stemmery z projektów Morfologik i Hunspell są
oparte na słownikach, Stempel zaś jest stemmerem algorytmicznym.

Przeprowadzono testy dla stemmera Stempel (wybranego ze względu na prędkość), w
ramach którego porównano zwykły wynik pracy stemmera z trzema innymi wersjami
tego wyniku poddanego mutacjom. Testy przeprowadzono dla kolejnych wartości
poziomu intensywności mutacji, którego wzrost determinuje większą ilość
wprowadzanych zmian. Jako wejścia stemmera użyto zbioru \texttt{dictionary.txt}.
\begin{center}
    \begin{tabular}{ | l | c | c | c | c | c | }
    \hline
                     & 10    & 20   & 30    & 50    & 80   \\ \hline
    Wynik stemmera   & 97\%  & 92\% & 88\%  & 82\%  & 80\% \\ \hline
    Wyniki mutowane  & 94\%  & 87\% & 82\%  & 76\%  & 76\% \\ \hline
    \end{tabular}
\end{center}
Dla każdego przypadku wszystkie mutowane wyniki otrzymały podobne oceny
jakości. Otrzymane wyniki wskazują, że dla niezerowych wartości poziomu
intensywności mutacji niezmutowane wyniki będą oceniane lepiej od mutowanych.
Choć mutowane wyniki są liczniejsze od poprawnego wyniku, to uzyskiwane mutacje
ze względu na identyczną intensywność wykazują rozłączność.

Wykonano także test dla stemmerów polegający na przeprowadzeniu porównania
oryginalnego wyniku stemmera z jego mutacjami generowanymi dla poziomów
intensywności mutacji wynoszących kolejno: 10, 30, 50, 70 i 100 jednocześnie.
Wejściem stemmera był plik \texttt{dictionary.txt}.
\begin{center}
    \begin{tabular}{ | l | c | c | c | c | c | c | }
    \hline
                    & 0    & 10    & 30   & 50    & 70    & 100   \\ \hline
    Morfologik      & 75\% & 70\%  & 64\% & 61\%  & 63\%  & 66\%  \\ \hline
    Hunspell        & 76\% & 70\%  & 64\% & 62\%  & 65\%  & 70\%  \\ \hline
    Stempel         & 86\% & 82\%  & 77\% & 76\%  & 77\%  & 83\%  \\ \hline
    \end{tabular}
\end{center}
Wyniki ponownie wskazują ,,właściwy'' stemmer jako najlepszy. Istotny w wynikach
wydaje się być wzrost jakości dla poziomu mutacji powyżej 50\%. Silnie zmutowane
wyniki zaniżają rezultaty dla bardziej poprawnych stemmerów. Ponadto mogą
sprawiać one , że pomyłki ze słabiej zmutowanych wyników mają większą szansę
powtórzyć się. Z tego powodu komparator uznaje ,,odstawanie'' poprawnego  wyniku
za sygnał świadczący o niższej jakości generującego go stemmera, a powtarzanie
błędu za cechę pozytywną.

\subsection*{Wniosek}
Zaimplementowana metoda porównania stemmerów wykazuje cechy mogące posłużyć do
określenia jej mianem użytecznej. Potrafi ona odróżniać lepsze stemmery od
gorszych Jej wadą jest niewielka różnica ocen dla różnych stemmerów. Potrafi ona
jednocześnie wskazywać wysokie wyniki dla całkowicie błędnych
stemmerów - powstawanie tak bardzo odmiennych od siebie wyników wyszukiwania
rdzeni jest jednak mało prawdopodobne w praktyce. Potencjalnie przydatną cechą
metody może być to, że do zastosowania nie potrzebuje ona korpusu bądź innych
specjalnie przygotowywanych wyników referencyjnych.

\begin{thebibliography}{}
\bibitem{dict} \emph{Lista słów do gier}. \url{http://www.sjp.pl/slownik/growy/}
\bibitem{wiki} \emph{Ustawa o działalności pożytku publicznego i o wolontariacie}. \url{http://pl.wikisource.org/wiki/Ustawa_o_dzia%C5%82alno%C5%9Bci_po%C5%BCytku_publicznego_i_o_wolontariacie}
\end{thebibliography}

\end{document}

