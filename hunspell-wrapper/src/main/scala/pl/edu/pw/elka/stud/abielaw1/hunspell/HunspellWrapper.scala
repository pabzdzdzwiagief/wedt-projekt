package pl.edu.pw.elka.stud.abielaw1.hunspell

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

import pl.edu.pw.elka.stud.abielaw1.stemmer.StemmerApp
import org.apache.lucene.analysis.hunspell.{HunspellDictionary, HunspellStemmer}
import org.apache.lucene.util.Version
import collection.JavaConversions

/** Obiekt-adapter dla stemmera z projektu Hunspell. */
object HunspellWrapper extends StemmerApp {
  import JavaConversions.seqAsJavaList

  lazy val stemmer = new HunspellStemmer(dictionary) with CanStem {
    def stem(cs: CharSequence) = {
      val stems = super.stem(cs.toString)
      if (stems.size > 0) stems.get(0).getStemString else "???"
    }
  }

  /** Obiekt słownika wymagany przez bibliotekę Hunspell. */
  lazy val dictionary = new HunspellDictionary(
    affixStream,
    dictionaryStreams,
    Version.LUCENE_36,
    true
  )

  /** Affiksy Hunspella. */
  lazy val affixStream = getClass getResourceAsStream "/pl_PL.aff"

  /** Słownik Hunspella. */
  lazy val dictionaryStreams = List(getClass getResourceAsStream "/pl_PL.dic")
}
