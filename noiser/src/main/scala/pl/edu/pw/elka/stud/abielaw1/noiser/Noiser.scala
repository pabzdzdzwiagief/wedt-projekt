package pl.edu.pw.elka.stud.abielaw1.noiser

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */


import pl.edu.pw.elka.stud.abielaw1.comparator.groups.{Word, WordGroup, WordGroupFactory}
import xml.XML
import xml.PrettyPrinter
import util.Random

/** Aplikacja wprowadzająca szum do wyjścia stemmera. */
object Noiser extends App {
  /** Wejściowy dokument XML.  */
  val input = XML load System.in

  /** Procentowy "poziom szumu". Im większy tym więcej zmian. */
  val noiseLevel = args(0).toInt

  /** Generator losowy. */
  private val random = new Random()

  /** @return true tym częściej, im mniejszy jest szum. */
  private def lessNoise = random.nextInt(100) >= noiseLevel

  /** @return true tym częściej, im większy jest szum. */
  private def moreNoise = random.nextInt(100) <  noiseLevel

  /** Grupy słów przekazane w wejściowym dokumencie XML. */
  val groups = (for (group <- input.child) yield
                  WordGroupFactory fromXML group).flatten

  /**
   * Konwersja nadająca możliwość losowego usunięcia lub dodania losowych
   * słów do zbioru.
   */
  private implicit def mutable(words: Set[Word]) = new {
    /** @return para (zbiór z usuniętymi słowami, usunięte słowa).*/
    def withWordsRemoved = {
      val less = (words.tail filter (_ => lessNoise)) + words.head
      val removed = words filterNot (less contains _)
      (less, removed)
    }

    /**
     * @param available - słowa możliwe do dodania.
     * @return para (zbiór z dodanymi słowami, nie-dodane słowa).
     */
    def withWordsInserted(available: Set[Word]) = {
      val inserted = available filter (_ => moreNoise)
      val remaining = available filterNot (inserted contains _)
      val more = words ++ inserted
      (more, remaining)
    }
  }

  /**
   * Mutuje grupy słów poprzez losowe dodawanie/usuwanie/przenoszenie słów.
   * @param rest - grupy do zmutowania.
   * @param moved - słowa dotychczas usunięte i możliwe do dodania gdzie indziej.
   * @param done - zmutowane już grupy.
   * @return - zmutowane grupy słów.
   */
  private def mutate(
      rest: Seq[WordGroup],
      moved: Set[Word] = Set(),
      done: List[WordGroup] = List()): List[WordGroup] = rest.headOption match {
    case Some(WordGroup(stem, words)) =>
      val (reduced, removed) = words.withWordsRemoved
      val available = moved ++ removed
      val (expanded, remaining) = reduced withWordsInserted available
      val mutatedGroup = WordGroup(stem, expanded)
      mutate(rest.tail, remaining, mutatedGroup :: done)
    case _ =>
      done
  }

  /**
   * "Skleja" sąsiednie grupy.
   * @param groups - grupy do przetworzenia.
   * @return - grupy, z których losowa część jest połączona ze sobą.
   */
  private def regroup(groups: List[WordGroup]) = {
    val pairs = groups.sliding(2, 2)
    val merged = pairs.map {
      case x :: y :: Nil =>
        if (moreNoise)
          List(WordGroup(x.stem, x.stemmed ++ y.stemmed))
        else
          List(x, y)
      case x :: Nil =>
        List(x)
      case Nil =>
        Nil
    }.flatten
    merged
  }

  /**
   * Grupy przetasowane, ze zmodyfikowanymi zawartościami, a następnie
   * połączone losowo z sąsiadami.
   */
  val mutatedGroups = regroup(mutate(random shuffle groups))

  /**
   * Rezultat wprowadzania szumu w postaci dokumentu XML. Format jest
   * identyczny z formatem wyjściowym adapterów stemmerów.
   */
  val fullResult = {
    <groups>
      { for (group <- mutatedGroups) yield
          <group stem={group.stem.text}>
            { for (word <- group.stemmed) yield
                <word>{word.text}</word>
            }
          </group>
      }
    </groups>
  }

  /** Wyprowadzenie na wyjście sformatowanego rezultatu wprowadzania szumu. */
  println(new PrettyPrinter(80, 4) format fullResult)
}
