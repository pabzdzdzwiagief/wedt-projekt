#!/usr/bin/env bash

LVL=$1
TEXT=$2

java -jar comparator/target/comparator-1.0.jar \
<(java -jar stempel-wrapper/target/stempel-wrapper-1.0.jar <$TEXT | tee /tmp/stempel.xml) \
<(java -jar stempel-wrapper/target/stempel-wrapper-1.0.jar <$TEXT | java -jar noiser/target/noiser-1.0.jar $LVL | tee /tmp/stempel-noise.1.xml) \
<(java -jar stempel-wrapper/target/stempel-wrapper-1.0.jar <$TEXT | java -jar noiser/target/noiser-1.0.jar $LVL | tee /tmp/stempel-noise.2.xml) \
<(java -jar stempel-wrapper/target/stempel-wrapper-1.0.jar <$TEXT | java -jar noiser/target/noiser-1.0.jar $LVL | tee /tmp/stempel-noise.3.xml) \
| tee /tmp/cmp.xml | grep "averageSimilarity"
