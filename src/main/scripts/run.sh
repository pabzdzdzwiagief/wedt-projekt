#!/usr/bin/env bash

TEXT=$1

java -jar comparator/target/comparator-1.0.jar \
<(java -jar hunspell-wrapper/target/hunspell-wrapper-1.0.jar <$TEXT     | tee /tmp/hunspell.xml) \
<(java -jar morfologik-wrapper/target/morfologik-wrapper-1.0.jar <$TEXT | tee /tmp/morfologik.xml) \
<(java -jar stempel-wrapper/target/stempel-wrapper-1.0.jar <$TEXT       | tee /tmp/stempel.xml) \
| tee /tmp/cmp.xml | grep "averageSimilarity"
