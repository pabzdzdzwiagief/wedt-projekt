package pl.edu.pw.elka.stud.abielaw1.stemmer

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

/**
 * Obiekt budujący mapowanie z rdzeni na słowa je posiadające.
 * Takie mapowania tworzą grupy słów o wspólnym rdzeniu.
 * @param toMap - mapowanie z rdzenia na posiadające go (wg stemmera) słowa.
 */
private[this] class Grouper(val toMap: Map[String, Set[String]] = Map()) {
  /**
   * Dodaje nową parę do mapowania.
   * @param stem - rdzeń.
   * @param originalWord - słowo dla którego odnaleziono ten rdzeń.
   * @return nowy obiekt budujący posiadający uaktualnione mapowanie.
   */
  def +(stem: String, originalWord: String) = {
    val newGroup = toMap.getOrElse(stem, Set[String]()) + (elem = originalWord)
    new Grouper(toMap.updated(stem, newGroup))
  }

  /** Postać XML reprezentująca zbudowane grupy słów. */
  lazy val toXML =  {
    <groups>
      { for (stemGroup <- toMap) yield
          <group stem={stemGroup._1}>
            { for (word <- stemGroup._2) yield
                <word>{word}</word>
            }
          </group>
      }
    </groups>
  }
}
