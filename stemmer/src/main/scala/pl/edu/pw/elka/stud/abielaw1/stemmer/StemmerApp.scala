package pl.edu.pw.elka.stud.abielaw1.stemmer

import io.Source
import xml.PrettyPrinter

/** Ogólna domieszka dla aplikacji-adapterów dla stemmerów. */
trait StemmerApp extends App {
  /** Pojedyncze słowa z wejścia oczyszczone z części zanków specjanych.  */
  val tokens = {
    val source = Source.fromInputStream(System.in)
    val tokenized = source
      .getLines()
      .reduce(_ + _)
      .map(c => if (Character isAlphabetic c) c else ' ')
      .split("\\s+")
    source.close()
    tokenized
  }

  /** Domieszka, którą musi implementować obiekt adaptowanego stemmera. */
  trait CanStem {
    /**
     * Znajduje rdzeń dla zadanego słowa.
     * @param cs - słowo.
     * @return - rdzeń słowa.
     */
    def stem(cs: CharSequence): CharSequence
  }

  def stemmer: CanStem

  /** Konwersja dodająca składową "rdzeń" dla każdego napisu. */
  implicit def `string to stemmable word`(word: String) = new {
    def stem = {
      val result = stemmer stem word
      if (result ne null) result.toString else "???"
    }
  }

  /** Grupuje słowa wokół znalezionych dla nich rdzeni. */
  private val grouper = tokens.foldLeft(new Grouper) {
    case (g, w) => g + (w.stem, w)
  }

  /** Wyprowadza na wyjście rezultat działania stemmera w postaci XML. */
  println(new PrettyPrinter(80, 4) format grouper.toXML)
}
