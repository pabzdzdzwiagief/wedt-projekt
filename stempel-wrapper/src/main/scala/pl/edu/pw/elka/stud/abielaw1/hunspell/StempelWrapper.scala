package pl.edu.pw.elka.stud.abielaw1.hunspell

/**
 * @author Aleksander Bielawski <A.J.Bielawski@stud.elka.pw.edu.pl>
 */

import org.apache.lucene.analysis.stempel.StempelStemmer
import org.apache.lucene.analysis.pl.PolishAnalyzer
import pl.edu.pw.elka.stud.abielaw1.stemmer.StemmerApp

/** Obiekt-adapter dla stemmera Stempel. */
object StempelWrapper extends StemmerApp {
  lazy val stemmer = new StempelStemmer(
    classOf[PolishAnalyzer] getResourceAsStream stemmerTable
  ) with CanStem

  /** Standardowa tablica umieszczona jako zasób. */
  lazy val stemmerTable = PolishAnalyzer.DEFAULT_STEMMER_FILE
}
